﻿using System;

namespace PD_Command
{
    class Program
    {
        static void Main(string[] args)
        {
            var empresa = new EmpresaInvoker();
            var producto = new ProductoReceiver();
            producto.Cantidad = 100;

            var ordenAlta = new AltaStockCommand(producto, 10);
            empresa.TomarOrden(ordenAlta);
            var ordenbaja = new BajaStockCommand(producto, 50);
            empresa.TomarOrden(ordenbaja);

            empresa.ProcesarOrdenes();

            Console.Write(string.Format("Total stock es {0}", producto.Cantidad));
            Console.ReadKey();
        }
    }
}
