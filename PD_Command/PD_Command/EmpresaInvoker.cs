﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PD_Command
{
    public class EmpresaInvoker
    {
        //Lista de ordenes 
        private List<OrdenCommand> ordenes = new List<OrdenCommand>();
        
        //Metodos

        //Llenamos la lista mediante la operacion TomarOrden
        public void TomarOrden(OrdenCommand cmd)
        {
            ordenes.Add(cmd);
        }

        //Recorremos toda la lista de ordenes 
        public void ProcesarOrdenes()
        {
            foreach (var orden in ordenes)
                orden.Ejecutar();

            //Vaciar lista para que quede disponible nuevamente para sus uso 
            ordenes.Clear();
        }
    }
}
