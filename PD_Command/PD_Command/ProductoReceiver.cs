﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PD_Command
{
    public class ProductoReceiver
    {
        //Atributos
        public double Cantidad { get; set; }
        public string Nombre { get; set; }

        //Implementa 2 operaciones propias del producto
        public void RestarStock(double cant)
        {
            Cantidad = Cantidad - cant;
            Console.WriteLine(string.Format("Quitando {0} unidades", cant));
        }

        public void SumarStock(double cant)
        {
            Cantidad = Cantidad + cant;
            Console.WriteLine(string.Format("Agregando {0} unidades", cant));
        }
    }
}
