﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PD_Command
{
    //Clase abstracta
    public abstract class OrdenCommand
    {

        //Metodo
        public abstract void Ejecutar();

        protected ProductoReceiver _producto;
        protected double _cantidad;

        //Constructor
        public OrdenCommand(ProductoReceiver producto, double cantidad)
        {
            _producto = producto;
            _cantidad = cantidad;
        }
        //Se va a implementar en 2 clases concretas AltaStockCommand y BajaStockCommand
    }
}
