﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PD_Command
{
    public class AltaStockCommand : OrdenCommand
    {
        //Construcctor 
        public AltaStockCommand(ProductoReceiver producto, double cantidad) : base(producto, cantidad)
        {
        }

        //Definimos que operacion va a ejecutar
        public override void Ejecutar()
        {
            _producto.SumarStock(_cantidad);
        }
    }
}
